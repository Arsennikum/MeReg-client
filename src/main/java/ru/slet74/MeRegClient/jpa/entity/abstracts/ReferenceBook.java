package ru.slet74.MeRegClient.jpa.entity.abstracts;

import lombok.Data;

import javax.persistence.*;

@Data
@MappedSuperclass
public abstract class ReferenceBook {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private boolean additional; // основное поле или дополнительное, добавленное юзерами
}
