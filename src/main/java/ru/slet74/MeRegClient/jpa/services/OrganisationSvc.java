package ru.slet74.MeRegClient.jpa.services;

import ru.slet74.MeRegClient.jpa.entity.Organisation;

/**
 * Created by Алексей on 08.05.2017.
 */
public interface OrganisationSvc {
    Organisation add(Organisation organisation);
}
