package ru.slet74.MeRegClient.services;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.repository.HumanRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Алексей on 03.06.2017.
 */
@Service
public class SpreadsheetSvc {
    private File file;
    private List<Human> humans = new LinkedList<>();
    private List<Human> organisations = new LinkedList<>();
    private List<Human> towns = new LinkedList<>();
    private final HumanRepository humanRepository;

    @Autowired
    public SpreadsheetSvc(HumanRepository humanRepository) {
        this.humanRepository = humanRepository;
    }

    public void parse(File file, boolean override) throws IOException {
        this.file = file;
        Workbook workBook = new XSSFWorkbook(new FileInputStream(file));
        Sheet sheet = workBook.getSheetAt(0);

        Iterator<Row> rows = sheet.rowIterator();
        while (rows.hasNext()) {
            Row currentRow = rows.next();
            String family, name, tel;

            family = currentRow.getCell(0).getStringCellValue();
            name = currentRow.getCell(1).getStringCellValue();
            tel = currentRow.getCell(2).getStringCellValue();

            Human human = humanRepository.findByFamilyAndNameAndTel(family, name, tel);
            if (human == null || override) {
                if (human == null) {
                    human = new Human();
                }
                human.setFamily(family);
                human.setName(name);
                human.setTel(tel);

                human.setOtch(currentRow.getCell(3).getStringCellValue());
                human.setEmail(currentRow.getCell(4).getStringCellValue());
            }
        }
    }


}
