package ru.slet74.MeRegClient.jpa.services;

import ru.slet74.MeRegClient.jpa.entity.Human;

import java.util.List;

/**
 * Created by Алексей on 08.05.2017.
 */
public interface HumanSvc {
    Human add(Human human);
    Human findById(long id);
    List<Human> findByFamilyAndName(String family, String name);

    List<Human> findAll();
}
