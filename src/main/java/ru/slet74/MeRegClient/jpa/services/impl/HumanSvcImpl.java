package ru.slet74.MeRegClient.jpa.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.repository.HumanRepository;
import ru.slet74.MeRegClient.jpa.services.HumanSvc;

import java.util.List;

/**
 * Created by Алексей on 08.05.2017.
 */
@Service("humanSvc")
public class HumanSvcImpl implements HumanSvc {
    private HumanRepository humanRepository;

    @Autowired
    public HumanSvcImpl(HumanRepository humanRepository) {
        this.humanRepository = humanRepository;
    }

    @Override
    public Human add(Human human) {
        return humanRepository.save(human);
    }

    @Override
    public Human findById(long id) {
        return humanRepository.findById(id);
    }

    @Override
    public List<Human> findByFamilyAndName(String family, String name) {
        return humanRepository.findByFamilyAndName(family, name);
    }

    @Override
    public List<Human> findAll() {
        return humanRepository.findAll();
    }
}
