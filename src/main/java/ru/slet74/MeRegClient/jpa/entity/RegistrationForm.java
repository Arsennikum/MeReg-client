package ru.slet74.MeRegClient.jpa.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Inheritance(strategy= InheritanceType.JOINED)
@DiscriminatorColumn(name="entity_type")
@DiscriminatorValue("DURING")
public class RegistrationForm {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "human", referencedColumnName = "id")
    private Human human;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "address", referencedColumnName = "id")
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinColumn(name = "organisation", referencedColumnName = "id")
    private Organisation organisation;

    @Column(name = "from_organisation")
    private String fromOrganisation;

    @Column(name = "work_place")
    private String workPlace;

    @OneToMany
    @Column(name = "directions")
    private List<Direction> directions;

    @Column(name = "comment_note")
    private String commentNote;
}
