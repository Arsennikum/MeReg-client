package ru.slet74.MeRegClient.jpa.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String region;
    private String district;
    private String city;
    private String street;
    private String house;
    private String housing;
    private String office;
    private String building;
}