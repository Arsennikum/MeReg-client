package ru.slet74.MeRegClient.jpa.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users", schema = "security")
@Data
@EqualsAndHashCode(of = "username")//exclude = {"password", "userRole"})
public class User {
    @Id
    @Column(nullable = false, unique = true)
    private String username;

    private String password;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private Set<UserRole> userRole = new HashSet<>(0);

    private boolean enabled;
}