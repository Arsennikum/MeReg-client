$(document).ready(function () {

    $("#main-form").find("#bth-search").click(function () {
        fire_ajax_submit();
    });

});

function editPayment(id) {
    document.getElementById('modalEdit').style.display='block';
    for (var pay in pays) {
        if (pays[pay]['id'] == id) {
            fillFields(pays[pay]);
        }
    }
}

function fire_ajax_submit() {

    var search = {};
    search["family"] = $("#family").val();
    search["name"] = $("#name").val();

    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/form/search",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 20000,
        success: function (data) {

            /*var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#feedback').html(json);*/

            document.getElementById('modalFound').style.display='block';

            var searchResult = "";
            arr = data.result;
            for(var i in arr) {
                searchResult += "<a href=\"#\" id=\"" + arr[i].id + "\">"
                        + arr[i].name + " " + arr[i].family + " " + arr[i].tel + "</a><br/>";
            }
            $('#searchResult').html(searchResult);

            for(var i in arr) {
                $('#' + arr[i].id).click(function() {
                    fillFields(arr[i]);
                    document.getElementById('modalFound').style.display='none';
                })
            }

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}

function fillFields(variable) {
    for(var key in variable) {
        var val = variable[key];

        if ($('#' + key).attr('type') == 'date') {
            val = new Date(val);
            var day = ("0" + val.getDate()).slice(-2);
            var month = ("0" + (val.getMonth() + 1)).slice(-2);

            val = val.getFullYear() + '-' + month + '-' + day;
        }
        if (($('#' + key).attr('type') == 'checkbox')&&(val)) {
            document.getElementById(key).setAttribute('checked', 'checked');
        }
        $('#' + key).val(val);
    }

    /*$('.human-fields').each(function() {
        this.val(human[this]);
    })*/
}