package ru.slet74.MeRegClient.web.ajax;

import ru.slet74.MeRegClient.jpa.entity.Human;

import java.util.List;

/**
 * Created by Алексей on 18.05.2017.
 */
public class SearchResponseBody {
    private String msg;
    List<Human> result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Human> getResult() {
        return result;
    }

    public void setResult(List<Human> result) {
        this.result = result;
    }
}
