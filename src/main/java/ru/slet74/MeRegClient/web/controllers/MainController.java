package ru.slet74.MeRegClient.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Алексей on 12.06.2017.
 */
@Controller
@RequestMapping("/")
public class MainController {

    @GetMapping(value = "/login")
    public String login() {
        return "/login";
    }

}
