package ru.slet74.MeRegClient.jpa.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Алексей on 12.06.2017.
 */
@Entity
@Data
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "human", nullable = false)
    private Human human;
    private Boolean completely;
    private Double count;
    private String comment;
}
