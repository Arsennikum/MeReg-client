package ru.slet74.MeRegClient.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.slet74.MeRegClient.jpa.entity.Organisation;

/**
 * Repository for {@link Organisation}
 */
@Repository
public interface OrganisationRepository extends CrudRepository<Organisation, Long> {
    Organisation findById(long id);
    Organisation findByName(String name);
}
