package ru.slet74.MeRegClient.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Алексей on 08.05.2017.
 */
@Controller
@RequestMapping("/manager")
public class ManagerController {

    @GetMapping
    public String home() {
        return "/form";
    }

    @PostMapping(value = "/reloadData")
    public String reloadData() {
        // лучше сделать подгрузку напрямую из гугл таблицы и оттуда импортировать
        return "/manager";
    }
}
