package ru.slet74.MeRegClient.jpa.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.entity.Payment;
import ru.slet74.MeRegClient.jpa.repository.PaymentRepository;
import ru.slet74.MeRegClient.jpa.services.PaymentSvc;

import java.util.List;

/**
 * Created by Алексей on 12.06.2017.
 */
@Service
public class PaymentSvcImpl implements PaymentSvc{
    private PaymentRepository paymentRepository;

    @Autowired
    public PaymentSvcImpl(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }

    public List<Payment> findByHuman(Human human) {
        return paymentRepository.findByHuman(human);
    }

    @Override
    public List<Payment> findAll() {
        return paymentRepository.findAll();
    }
}
