package ru.slet74.MeRegClient.web.ajax;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Entity for ajax request for search
 */
public class SearchCriteria {
    @NotBlank(message = "Напишите вашу фамилию")
    private String family;
    @NotBlank(message = "Напишите ваше имя")
    private String name;

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
