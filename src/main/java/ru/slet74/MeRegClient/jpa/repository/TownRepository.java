package ru.slet74.MeRegClient.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.slet74.MeRegClient.jpa.entity.Town;

/**
 * Repository for {@link Town}
 */
@Repository
public interface TownRepository extends CrudRepository<Town, Long> {
    Town findById(long id);
}
