package ru.slet74.MeRegClient.jpa.services;

import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.entity.Payment;

import java.util.List;

/**
 * Created by Алексей on 12.06.2017.
 */
public interface PaymentSvc {
    Payment save(Payment payment);
    List<Payment> findByHuman(Human human);
    List<Payment> findAll();
}
