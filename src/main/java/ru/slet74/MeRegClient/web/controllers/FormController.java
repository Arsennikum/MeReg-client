package ru.slet74.MeRegClient.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.slet74.MeRegClient.web.ajax.SearchCriteria;
import ru.slet74.MeRegClient.web.ajax.SearchResponseBody;
import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.services.HumanSvc;
import ru.slet74.MeRegClient.web.models.Msg;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Алексей on 08.05.2017.
 */
@Controller
@RequestMapping("/form")
public class FormController {
    private HumanSvc humanSvc;

    @Autowired
    public FormController(HumanSvc humanSvc) {
        this.humanSvc = humanSvc;
    }

    @GetMapping
    public ModelAndView home() {
        return new ModelAndView("form", "command", new Human());
    }

    @GetMapping(value = "/test")
    public String test() {
        return "redirect:/manager";
    }

    @PostMapping(value = "/search")
    public ResponseEntity<?> search(@Valid @RequestBody SearchCriteria search, Errors errors) {
        SearchResponseBody result = new SearchResponseBody();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {
            result.setMsg(errors.getAllErrors()
                    .stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);
        }

        List<Human> humans = humanSvc.findByFamilyAndName(search.getFamily(), search.getName());
        if (humans.isEmpty()) {
            result.setMsg("Не найдено");
        } else {
            result.setMsg("Найдено " + humans.size() + " записей");
        }
        result.setResult(humans);

        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "/saveHuman")
    public String saveHuman(@Valid @ModelAttribute Human human, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("msg", new Msg(bindingResult.getAllErrors()
                    .stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(", "))));
            return "fragments/savingResult :: info-errors";
        }
        try {
            humanSvc.add(human);
        } catch(Exception e) {
            e.printStackTrace();
            model.addAttribute("msg", new Msg(e.getMessage()));
            return "fragments/savingResult :: info-errors";
        }
        if (human.getVolunteer() != null && human.getVolunteer()) {
            return "fragments/savingResult :: info-volunteer";
        } else {
            return "fragments/savingResult :: info-toPay";
        }
    }
}
