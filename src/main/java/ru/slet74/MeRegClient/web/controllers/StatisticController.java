package ru.slet74.MeRegClient.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.services.HumanSvc;
import ru.slet74.MeRegClient.utils.ChartData;
import ru.slet74.MeRegClient.utils.KeyValue;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Алексей on 12.06.2017.
 */
@Controller
@RequestMapping("/statistic")
public class StatisticController {
    private HumanSvc humanSvc;

    @Autowired
    public StatisticController(HumanSvc humanSvc) {
        this.humanSvc = humanSvc;
    }

    @GetMapping
    public String home() {
        return "statistic";
    }

    @PostMapping(value = "/data")
    public ResponseEntity<?> getData() {
        List<Human> humans = humanSvc.findAll();
        long volunteers = humans.stream()
                .filter(human -> (human.getVolunteer() != null && human.getVolunteer())).count();
        List<KeyValue> data = new LinkedList<>();
        data.add(new KeyValue("Волонтёры", volunteers));
        data.add(new KeyValue("Оплата", humans.size() - volunteers));
        ChartData chartData = new ChartData("Волонтёры", data);
        return ResponseEntity.ok(chartData);
    }

}
