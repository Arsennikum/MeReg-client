package ru.slet74.MeRegClient.jpa.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.slet74.MeRegClient.jpa.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}