package ru.slet74.MeRegClient.jpa.repository;

/**
 * Created by Алексей on 08.05.2017.
 */

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.slet74.MeRegClient.jpa.entity.Human;

import java.util.List;

/**
 * Repository for {@link Human}
 */
@Repository
public interface HumanRepository extends CrudRepository<Human, Long> {
    Human findById(long id);
    @Query("select h from Human h where h.family like ?1% and h.name like ?2%")
    List<Human> findByFamilyAndName(String family, String name);
    List<Human> findAll();
    Human findByFamilyAndNameAndTel(String family, String name, String tel);
}
