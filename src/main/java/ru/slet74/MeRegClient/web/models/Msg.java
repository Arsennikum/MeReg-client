package ru.slet74.MeRegClient.web.models;

/**
 * Created by Алексей on 22.05.2017.
 */
public class Msg {
    private String msg;

    public Msg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
