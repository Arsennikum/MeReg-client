package ru.slet74.MeRegClient.utils;

import lombok.Data;

import java.util.List;

/**
 * Created by Алексей on 27.06.2017.
 */
@Data
public class ChartData {
    String name;
    List<KeyValue> data;

    public ChartData(String name, List<KeyValue> data) {
        this.name = name;
        this.data = data;
    }
}
