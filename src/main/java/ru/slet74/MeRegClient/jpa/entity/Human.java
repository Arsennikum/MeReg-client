package ru.slet74.MeRegClient.jpa.entity;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Алексей on 06.05.2017.
 */
@Data
@Entity
@Table(
        //name = Human.TABLE_NAME,
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "family", "tel"})}
        )
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
/*@DiscriminatorColumn(name="entity_type")
@DiscriminatorValue("HUMAN")*/
public class Human {
    public static final String TABLE_NAME = "people";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "family") @NotEmpty
    private String family;
    @Column(name = "name")@NotEmpty
    private String name;
    @Column(name = "otch")
    private String otch;
    @DateTimeFormat(pattern="yyyy-MM-dd") @Column(name = "birthday") @NotEmpty
    private Date birthday;
    @Column(name = "tel") @NotEmpty
    private String tel;
    @Email @NotEmpty @Column(name = "email")
    private String email;
    @Column(name = "volunteer")
    private Boolean volunteer = false;


}
