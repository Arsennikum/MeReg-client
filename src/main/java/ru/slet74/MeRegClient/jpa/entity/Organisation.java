package ru.slet74.MeRegClient.jpa.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * Created by Алексей on 07.05.2017.
 */
@Entity
@Table(
        //name = Organisation.TABLE_NAME,
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})}
)
@Data
public class Organisation {
    public static final String TABLE_NAME = "organisation";

    //region Fields
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "name")
    private String name;
    //endregion
}
