package ru.slet74.MeRegClient.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import ru.slet74.MeRegClient.jpa.entity.Human;
import ru.slet74.MeRegClient.jpa.entity.Payment;

import java.util.List;

/**
 * Created by Алексей on 12.06.2017.
 */
public interface PaymentRepository extends CrudRepository<Payment, Long> {
    List<Payment> findAll();
    List<Payment> findByHuman(Human human);
}
