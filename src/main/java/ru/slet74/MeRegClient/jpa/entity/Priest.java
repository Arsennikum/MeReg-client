package ru.slet74.MeRegClient.jpa.entity;

import lombok.Data;

import javax.persistence.*;

@DiscriminatorValue("PRIEST")
public class Priest extends Human {
    @Column(name = "san")
    private String san;
}
