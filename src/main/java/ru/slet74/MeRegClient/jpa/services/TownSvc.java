package ru.slet74.MeRegClient.jpa.services;

import ru.slet74.MeRegClient.jpa.entity.Town;

/**
 * Created by Алексей on 08.05.2017.
 */
public interface TownSvc {
    Town add(Town town);
}
