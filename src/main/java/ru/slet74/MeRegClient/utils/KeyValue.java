package ru.slet74.MeRegClient.utils;

import lombok.Data;

/**
 * Created by Алексей on 27.06.2017.
 */
@Data
public class KeyValue {
    private String key;
    private Long value;

    public KeyValue(String key, Long value) {
        this.key = key;
        this.value = value;
    }
}
