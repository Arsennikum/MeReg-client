package ru.slet74.MeRegClient.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.slet74.MeRegClient.jpa.entity.User;
import ru.slet74.MeRegClient.jpa.entity.UserRole;
import ru.slet74.MeRegClient.jpa.repository.UserRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly=true)
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(), user.getPassword(), user.isEnabled(),
                true, true, true,
                buildUserAuthority(user.getUserRole())
        );
        //return new MyUserPrincipal(user);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {
        List<GrantedAuthority> authList = new LinkedList<>();

        // Build user's authorities
        for (UserRole userRole : userRoles) {
            authList.add(new SimpleGrantedAuthority(userRole.getRole()));
        }

        return authList;
    }

}