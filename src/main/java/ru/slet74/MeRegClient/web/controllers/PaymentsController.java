package ru.slet74.MeRegClient.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.slet74.MeRegClient.jpa.services.PaymentSvc;

/**
 * Created by Алексей on 08.05.2017.
 */
@Controller
@RequestMapping("/payments")
public class PaymentsController {
    private final PaymentSvc paymentSvc;

    @Autowired
    public PaymentsController(PaymentSvc paymentSvc) {
        this.paymentSvc = paymentSvc;
    }


    @PostMapping(value = "/reloadData")
    public String reloadData() {
        return "/payments";
    }

    @GetMapping
    public String home(Model model) {
        model.addAttribute("payments", paymentSvc.findAll());
        return "/payments";
    }
}
